# open and parse bandit.yml

require 'yaml'

def all_rule_ids(rules)
    rules['rules'].map do |r|
        if !r['id'].include?('-')
            r['id'] + "-1"
        else
            r['id']
        end
    end
end

def ids_without(s, rules)
    without = []
    rules['rules'].each do |r|
        unless /-[0-9]/.match(r['id'])
            without.append(r['id'])
        end
    end
    without
end

file_name = 'security_code_scan.yml'
sast_rules = YAML.load_file("../sast-rules/dist/#{file_name}")
semgrep_rules = YAML.load_file("../semgrep/rules/#{file_name}")

# number of rules
pp "SEMGREP TOTAL RULES:", semgrep_rules['rules'].length()
pp "SAST TOTAL RULES: ", sast_rules['rules'].length()

# # differences rules
pp "SEMGREP - SAST"
pp all_rule_ids(semgrep_rules) - all_rule_ids(sast_rules)

pp "SAST - SEMGREP"
pp all_rule_ids(sast_rules) - all_rule_ids(semgrep_rules)

# pp "SAST - SEMGREP"
# pp all_rule_ids(sast_rules) - all_rule_ids(semgrep_rules)

# compare primary IDS for ids_without("-1", semgrep_rules)
# ids_without("-1", semgrep_rules).each do |semgrep_id|
#     semgrep_rule = semgrep_rules['rules'].select{|r| r['id'] == semgrep_id}
#     sast_rule = sast_rules['rules'].select{|r| r['id'] == "#{semgrep_id}-1"}
#     pp semgrep_rule[0]['metadata']['primary_identifier']
#     pp sast_rule[0]['metadata']['primary_identifier']
#     pp ""
# end
