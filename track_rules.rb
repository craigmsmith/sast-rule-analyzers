require 'yaml'
require 'pp'

def display(results)
    results.each do |k, v|
        pp k
        pp v
        pp "***********"
    end
end

file_name = 'bandit.yml'
# file_name = 'security_code_scan.yml'
sast_rules = YAML.load_file("../sast-rules/dist/#{file_name}")
semgrep_rules = YAML.load_file("../semgrep/rules/#{file_name}")

# build a map of key semgrep rule patterns

search = {}
{semgrep: semgrep_rules, sast: sast_rules}.each do |k, rules|
    rules['rules'].each do |r|
        key = ""
        if r['pattern']
            key = r['pattern']
        end

        if r['patterns']
            key = PP.singleline_pp(r['patterns'], "")
        end
            
        if r['pattern-either']
            key = PP.singleline_pp(r['pattern-either'], "")
        end

        if !search.key?(key)
            search[key] = {
                sast: {
                    id: "",
                    primary_identifier: "",
                    secondary_identifiers: "",
                },
                semgrep: {
                    editted_id: "",
                    id: "",
                    primary_identifier: "",
                    secondary_identifiers: "",
                }
            }
        end
        search[key][k][:id] = r['id']
        if k == :semgrep
            editted_id = r['id']
            # all single sast-rules end in -1 but semgrep rules do not
            # this chnage updates the semgrep rules to match the sast rule IDs
            unless /-[0-9]$/.match(r['id'])
                editted_id = "#{r['id']}-1"
            end
            search[key][k][:editted_id] = editted_id
        end
        search[key][k][:primary_identifier] = r['metadata']['primary_identifier']

        search[key][k][:secondary_identifiers] = r['metadata']['secondary_identifiers'].map{|id| id['value']}.sort.join(",")
    end
end

# Show everything that's changed
changes = search.select { |k, v|
    v[:semgrep][:editted_id] != v[:sast][:id] &&
    v[:semgrep][:primary_identifier] != v[:sast][:primary_identifier]
    v[:semgrep][:secondary_identifiers] != v[:sast][:secondary_identifiers]
}.select{ |k, v| v[:semgrep][:editted_id] != "" && v[:sast][:id] != "" }
display(changes)
